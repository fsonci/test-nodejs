import { connect } from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel'
import cors from 'cors'

import {
  notFoundErrorHandler,
  unauthorizedErrorHandler,
  forbiddenErrorHandler,
  badRequestErrorHandler,
  catchAllErrorHandler,
} from './errorHandling'

import { check, validationResult } from 'express-validator'
const validationParameters = [
  check('email').isEmail().withMessage('please enter a valid email address'),
  check('firstName').exists().withMessage('please enter your name'),
  check('lastName').exists().withMessage('please enter your last name'),
  check('password').exists().withMessage('please enter your password'),
]

//connects to yakkyofy Database
connect(
  'mongodb+srv://test-user:17L2WY9bUWXqQmn3bnfPg8lB@maincluster.kwdmr.gcp.mongodb.net/yakkyofy?retryWrites=true&w=majority',
  { useNewUrlParser: true }
)

//creates an express instance
const app = express()

//allows cross origin requests
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

//test function
app.get('/', (req, res) => {
  res.send('Just a test')
})

//fetches all the entries in the users collection
app.get('/users', async (req, res, next) => {
  try {
    //converting query strings into ints for pagination parameters
    const skip = parseInt(req.query.skip, 10)
    const limit = parseInt(req.query.limit, 10)

    const results = await UserModel.find().skip(skip).limit(limit)
    if (results) {
      res.send(results)
    } else {
      console.log('problems fetching results')
    }
  } catch (error) {
    next(error)
  }
})

//adds a new user to the users collection in the database. Parameter entries are all validated with express-validator middleware
app.post('/users', validationParameters, async (req, res, next) => {
  try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const err = new Error()
      err.message = errors
      err.httpStatusCode = 400
      next(err)
    } else {
      let user = new UserModel()

      user.email = req.body.email
      user.firstName = req.body.firstName
      user.lastName = req.body.lastName
      user.password = req.body.password

      user.save((err, newUser) => {
        if (err) res.send(err)
        else res.status(201).send(newUser)
      })
    }
  } catch (error) {
    next(error)
  }
})

//modifies user data, by replacing all data with object coming form the body of the request. Parameter entries are all validated with express-validator middleware
app.put('/users/:userId', validationParameters, async (req, res, next) => {
  try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      const err = new Error()
      err.message = errors
      err.httpStatusCode = 400
      next(err)
    } else {
      const user = await UserModel.findByIdAndUpdate(req.params.userId, { $set: req.body })
      if (user) {
        res.send({ updated: user._id })
      } else {
        console.log("couldn't find user")
      }
    }
  } catch (error) {
    next(error)
  }
})

//deletes the user associated with the given objectId
app.delete('/users/:userId', async (req, res, next) => {
  try {
    const user = await UserModel.findByIdAndDelete(req.params.userId)
    if (user) {
      res.send('deleted')
    } else {
      const err = new Error('user to delete not found')
      err.httpStatusCode = 404
      next(err)
    }
  } catch (error) {
    next(error)
  }
})

//error handlers middlewares goes for last
app.use(badRequestErrorHandler)
app.use(notFoundErrorHandler)
app.use(forbiddenErrorHandler)
app.use(unauthorizedErrorHandler)
app.use(catchAllErrorHandler)

app.listen(8080, () => console.log('Example app listening on port 8080!'))
